from itertools import product

'''
Write a program that will take integer n as input and will
produce a mathematical expression whose solution is n.
The solution is restricted to using only four 4's and three of
the four binary operations (+,-,*,/). Division is truncated.
Assume the usual precendence of operations. Print 'no solution'
if n can not be produced by an expression.

For example
n = 0, a solution is 4 * 4 - 4 * 4 = 0.
n = 7, a solution is 4 + 4 - 4 / 4 = 7.
'''

runtimes = 0
while (runtimes<1 or runtimes>1000):
    while True:
        try:
            runtimes = int(input())
        except ValueError:
           continue
        else:
            break

listval = list()    # the list of answer values
i=0
while (i<runtimes):
    while True:
        try:
            val = int(input())
        except ValueError:
            continue
        else:
            break
    if (val >= -1000000) and (val <= 1000000):
        listval.append(val)
        i+=1

operations = ['+','-','*','//']
allarithoptions = list()    # operation variation tuples, ('+','+','+')
for i in product(operations, repeat=3):
    allarithoptions.append(i)

allcompleteoptions = list() # complete equation strings, '4 + 4 + 4 + 4'
for i in allarithoptions:
    allcompleteoptions.append('4 '+i[0]+' 4 '+i[1]+' 4 '+i[2]+' 4')

for answer in listval:
    checker = False
    for equation in allcompleteoptions:
        if eval(equation) == answer:
            equation = equation.replace('//','/')
            print(equation+' = '+ str(answer))
            checker = True
            break
    if checker==False:
        print('no solution')